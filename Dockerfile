FROM alpine:latest

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

RUN mkdir -p /usr/local/etc \
	&& { \
		echo 'install: --no-document'; \
		echo 'update: --no-document'; \
		echo ':backtrace: true' ; \
	} >> /usr/local/etc/gemrc

ADD Gemfile .

ENV BUILD_PACKAGES binutils binutils-libs gcc make libc-dev ruby-dev libffi-dev libxml2-dev libxslt-dev zlib-dev musl-dev gmp-dev
ENV RUBY_PACKAGES ruby

RUN set -ex && \
    apk update && \
    apk upgrade && \
    apk --no-cache add ca-certificates libressl libcurl $BUILD_PACKAGES $RUBY_PACKAGES && \
    gem update --system --no-post-install-message --no-document && \
    gem install --no-user-install --no-post-install-message --no-document bundler && \
    gem install --no-user-install --no-post-install-message --no-lock --file Gemfile && \
    apk del $BUILD_PACKAGES && \
    rm -rf -- /var/cache/apk/* /var/lib/apk/* /etc/apk/cache/* && \
    rm -rf -- Gemfile* && \
    unset BUILD_PACKAGES BUILD_PACKAGES && \
    ruby --version
