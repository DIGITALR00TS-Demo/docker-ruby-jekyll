# Ruby-Jekyll

Builds a Docker "container" using the offical Ruby Alpine container and preinstalling Jekyll, some pluggins, and utilities.
Then publishes the container to Docker Hub.

I needed something to simplify and speed up the CI for my demo.